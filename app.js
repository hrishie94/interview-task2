const express = require('express');
const app = express();
const { Server } = require('socket.io');
const http = require('http');
const server = http.createServer(app);
const io = new Server(server);
const port = 5000;
const authSocketMiddleware = require("./middleware/socketAuth")
app.get('/', (req, res) => {
	res.sendFile(__dirname + '/index.html');
});

// io.use((socket, next) => {
//   if (isValid(socket.request)) {
//     next();
//   } else {
//     next(new Error("invalid"));
//   }
// });
// socketServer = (server) => {
//   const io = require("socket.io")(server, {
//     cors: {
//       origin: "*",
//       methods: ["GET", "POST"],
//     },
//   });
io.use((socket, next) => {
  console.log("socket::", socket.id);
  const {token} = socket.handshake.headers
  if(token && token==="bearer 123"){
    return next()
  }
  next(new Error("new error please login"))
})
io.on('connection', (socket) => {
	socket.on('send name', (username) => {
		io.emit('send name', (username));
	});

	socket.on('send message', (chat) => {
		io.emit('send message', (chat));
	});
});
// }

server.listen(port, () => {
	console.log(`Server is listening at the port: ${port}`);
});
